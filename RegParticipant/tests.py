from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import partisipan
from .models import Peserta, Peserta_detail
from .forms import Peserta_Form

class RegParticipantUnitTest(TestCase):
    def test_urls_url_is_exist(self):
        #buat tes routing (urls)
        response = Client().get('/RegParticipant/')
        self.assertEqual(response.status_code,302)
        
    # def test_urls_using_RegParticipant_template(self):
    #     #buat tes itu ngerender html apa
    #     response = Client().get('/RegParticipant/')
    #     self.assertTemplateUsed(response, 'RegParticipant.html')

    def test_urls_using_partisipan_func(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/RegParticipant/')
        self.assertEqual(found.func, partisipan)

    def test_form_validation_for_blank_items(self):
        form_invalid = Peserta_Form(data={'event': ''})
        self.assertFalse(form_invalid.is_valid())
        self.assertEqual(
            form_invalid.errors['event'],
            ["This field is required."]
        )

    def test_new_status(self):
        new_activity = Peserta.objects.create(event = "Liburan Sekolah", detail = "Liburan Sekolah", username = "kar", email = "kar@gmail.com")
        counting_all_available_activity = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

