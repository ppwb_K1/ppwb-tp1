from django.db import models
from django import forms
from .models import Peserta, Peserta_detail

class Peserta_Form(forms.Form):
    Event = (
        ('LEARN SAP BW ON HANA SELF PACED TRAINING-SAP BW', 'LEARN SAP BW ON HANA SELF PACED TRAINING-SAP BW'),
        ('THE 26TH INDOCOMTECH 2018', 'THE 26TH INDOCOMTECH 2018'),
        ('CLASSIC HALLOWEEN NIGHT WITH NAIF', 'CLASSIC HALLOWEEN NIGHT WITH NAIF'),
        ('RCTI REDS RUN 2018',  'RCTI REDS RUN 2018'),
    ) 
    event = forms.CharField(label = 'Event',required = True, max_length = 300,widget = forms.Select(choices = Event))