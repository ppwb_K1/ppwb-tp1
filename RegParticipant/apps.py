from django.apps import AppConfig


class RegparticipantConfig(AppConfig):
    name = 'RegParticipant'
