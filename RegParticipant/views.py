from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib import messages
import TP1
from .models import Peserta_detail, Peserta
from .forms import Peserta_Form


response ={}
def partisipan(request):
	if request.user.is_authenticated:
		usernames = Peserta.objects.all().order_by('-id')
		response['usernames'] = usernames
		response['formpeserta'] = Peserta_Form
		html = 'RegParticipant.html'
		
		# print(request.session["daftar_event"])
		# print(request.session["email"])

		if (request.method == 'POST'):
			response['event'] = request.POST['event']
			text = request.session["daftar_event"]
			text.append(response['event'])
			request.session['daftar_event'] = text

			print(request.session["daftar_event"])

			usernameUser = request.session["username"]
			save_peserta = Peserta(event = response['event'], detail = request.session['daftar_event'], username = usernameUser, email = request.session['email'])
			save_peserta.save()
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/')

