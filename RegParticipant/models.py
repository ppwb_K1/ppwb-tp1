from django.db import models

class Peserta(models.Model):
    event = models.CharField(max_length=300)
    detail = models.CharField(max_length=3000)
    username = models.CharField(max_length=300)
    email = models.CharField(max_length=300)


class Peserta_detail(models.Model):
    username = models.CharField(max_length=300)
    email = models.CharField(max_length=350)
    daftar_event = models.CharField(max_length=30000)