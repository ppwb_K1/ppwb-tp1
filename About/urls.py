from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views as auth_views

from django.contrib.auth import views
from . import views


app_name = "About"

urlpatterns = [
    url(r"form_testi/", views.form, name='testi'),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'is_login/', views.is_login, name='is_login'),
    url(r'', views.about, name='About'),
]
