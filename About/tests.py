from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import about, form, is_login
from .models import Testimoni
from .forms import Form_Testimoni
from django.core.exceptions import ValidationError

class AboutUnitTest(TestCase):
    def test_urls_url_is_exist_About(self):
        #buat tes routing (urls)
        response = Client().get('/About/')
        self.assertEqual(response.status_code,200)

    def test_urls_using_About_template(self):
        #buat tes itu ngerender html apa
        response = Client().get('/About/')
        self.assertTemplateUsed(response, 'about.html')

    def test_urls_using_Testimoni_func(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/About/')
        self.assertEqual(found.func, about)

    def test_urls_url_is_exist_About2(self):
        #buat tes routing (urls)
        response = Client().get('/About/form_testi/')
        self.assertEqual(response.status_code,302)

    # def test_urls_using_About_template2(self):
    #     #buat tes itu ngerender html apa
    #     response = Client().get('/About/form_testi/')
    #     self.assertTemplateUsed(response, 'commentforms.html')

    def test_urls_using_Testimoni_func2(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/About/form_testi/')
        self.assertEqual(found.func, form)

    def test_form_validation_for_blank_items(self):
        form_invalid = Form_Testimoni(data={'Comment': ''})
        self.assertFalse(form_invalid.is_valid())
        self.assertEqual(
            form_invalid.errors['Comment'],
            ["This field is required."]
        )

    def test_form_registration_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response = Client().post('/About/', {'Comment': ''})
        self.assertEqual(response.status_code, 200)

        response= Client().get('/About/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_form_registration_post_error_and_render_the_result2(self):
        test = 'Anonymous'
        response = Client().post('/About/form_testi/', {'Comment': ''})
        self.assertEqual(response.status_code, 302)

        response= Client().get('/About/form_testi/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    

    