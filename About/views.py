from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from .models import Testimoni
from .forms import Form_Testimoni
import json


response ={}

def about(request):
	komen = Testimoni.objects.all().order_by('-id')
	response['komen'] = komen
	response['formtesti'] = Form_Testimoni
	html = 'about.html'
	form = Form_Testimoni(request.POST or None)
	
	if (request.method == 'POST'):
		response['Comment'] = request.POST['Comment']
		
		
		save_data = Testimoni(Comment = response['Comment'])
		save_data.save()
		save_data = Testimoni.objects.all().order_by('-id')
		return render(request, html, response)
	return render(request, html, response)

def form(request):
	response = {}
	if not request.user.is_authenticated:
		return HttpResponseRedirect("/About/")
		
	response["formtesti"] = Form_Testimoni
	return render(request, 'commentforms.html', response)


def is_login(request):
	print("is_login")
	if  request.user.is_authenticated and "form" in request.session:
		print("is_authenticated")
		if len(request.session["form"]) == 0:
			return JsonResponse({
				'event': "You have'nt registered for any events yet",
				'status':'success'
			})
		else:
			return JsonResponse({
				'event': request.session["form"],
				'status':'success'
			})

	elif request.user.is_authenticated and "form" not in request.session:
		request.session["form"]=[]
		return JsonResponse({
			'event': "You have'nt registered for any events yet",
			'status':'success'
		})

	else:
		print("Not_authenticated")
		return JsonResponse({
			'event': "-",
			'status':'fail'
		})
