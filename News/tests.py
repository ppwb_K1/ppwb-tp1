from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import display_News

class NewsUnitTest(TestCase):
    def test_urls_url_is_exist(self):
        #buat tes routing (urls)
        response = Client().get('/News/')
        self.assertEqual(response.status_code,200)

    def test_urls_using_News_template(self):
        #buat tes itu ngerender html apa
        response = Client().get('/News/')
        self.assertTemplateUsed(response, 'News.html')

    def test_urls_using_display_News_func(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/News/')
        self.assertEqual(found.func, display_News)


# Create your tests here.
