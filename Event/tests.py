from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .models import *
from RegParticipant.models import Peserta_detail
import unittest

class EventUnitTest(TestCase):
    # def setUp(self):
    #     user = User.objects.create_superuser(username="admin", email="admin@gmail.com", password="admin")
    #     self.user = authenticate(username='admin', password='admin')

    #     self.client = Client()
    #     self.logged_in = self.client.login(username = self.user.username, password = self.user.password)
        

    def test_urls_url_is_exist(self):
        #buat tes routing (urls)
        response = Client().get('/Event/')
        self.assertEqual(response.status_code,200)

    def test_urls_using_event_template(self):
        #buat tes /TP1 itu ngerender html apa
        response = Client().get('/Event/')
        self.assertTemplateUsed(response, 'Event.html')

    def test_urls_using_display_event_func(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/Event/')
        self.assertEqual(found.func, views.display_Event)

    def test_view_display_event_make_object_models_func(self):
        # session = self.client.session
        response = Client().get("/Event/display_Event/")
        count = len(event_list.objects.all())
        self.assertEqual(count, 0)

        event_list.objects.create(
            judul = "hello" ,
            tempat = "pacil",
            deskripsi = "mainmainmainmain",
            )

        response = Client().get("/Event/display_Event/")
        countt = len(event_list.objects.all())
        self.assertEqual(countt, 1)

    def test_view_list_daftar_event_not_login_json(self):
        response = Client().get("/Event/list_daftar_event/")
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'event' : ["You have'nt registered for any events yet"]})

    def test_view_is_login_not_login_json(self):
        response = Client().get("/Event/is_login/")
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'event': '-', 'status': 'fail'})
# Create your tests here.
    
    def  test_view_display_event_not_login(self):
        count1 = len(Peserta_detail.objects.all())
        self.assertEqual(count1, 0)