from django.shortcuts import render
from .models import event_list
from RegParticipant.models import Peserta_detail
from django.contrib import messages

from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import json

def display_Event(request):
	print('display')
	if request.user.is_authenticated :
		print("tes")
		request.session["email"] = request.user.email
		request.session["username"] = request.user.first_name + " " + request.user.last_name

		try:
			if(request.session["daftar_event"] == []): #disini error kalo pertama kali buat, karena gk ada keynya
				peserta = Peserta_detail.objects.all().filter(email=request.session["email"]).values()

				if len(peserta) == 0:
					Peserta_detail.objects.create(
						username=request.session["username"],
						email=request.session["email"],
						daftar_event="$"
						)
					request.session["daftar_event"] = []

				else:
					peserta = peserta[0]
					list_event = peserta['daftar_event'].split("$")
					request.session["daftar_event"] = list_event

				messages.success(request,"Hallo "+ request.session["username"])

		except KeyError:
			peserta = Peserta_detail.objects.all().filter(email=request.session["email"]).values()

			if len(peserta) == 0:
				Peserta_detail.objects.create(
					username=request.session["username"],
					email=request.session["email"],
					daftar_event="$"
					)
				request.session["daftar_event"] = []

			else:
				peserta = peserta[0]
				list_event = peserta['daftar_event'].split("$")
				request.session["daftar_event"] = list_event


			messages.success(request,"Hallo "+ request.session["username"])

	response = {}
	eventt = event_list.objects.all()
	
	response = {"eventt" : eventt}
	return render(request,'Event.html', response)


def list_daftar_event(request):
	event = []

	if not request.user.is_authenticated:
		event = ["You have'nt registered for any events yet"];

	if "daftar_event" in request.session:
		if len(request.session["daftar_event"]) == 0:
			event = ["You have'nt registered for any events yet"];

		else:
			event = request.session["daftar_event"]

	return JsonResponse({"event" : event})				



def is_login(request):
	print("is_login")
	if  request.user.is_authenticated and "daftar_event" in request.session:
		print("is_authenticated")
		if len(request.session["daftar_event"]) == 0:
			return JsonResponse({
				'event': "You have'nt registered for any events yet",
				'status':'success'
			})
		else:
			return JsonResponse({
				'event': request.session["daftar_event"],
				'status':'success'
			})

	elif request.user.is_authenticated and "daftar_event" not in request.session:
		request.session["daftar_event"]=[]
		return JsonResponse({
			'event': "You have'nt registered for any events yet",
			'status':'success'
		})

	else:
		print("Not_authenticated")
		return JsonResponse({
			'event': "-",
			'status':'fail'
		})