from django.conf.urls import url
from . import views

app_name = "Event"
urlpatterns = [
	url(r'is_login/', views.is_login, name='is_login'),
	url(r'list_daftar_event', views.list_daftar_event, name='list_daftar_event'),
	url(r'', views.display_Event, name='Event_'),
]
