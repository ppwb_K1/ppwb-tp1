$(function($) {
    
  $('dd[name="hide"]').hide();
  var allPanels = $('dd');
    
  $('dt a').click(function() {
    allPanels.slideUp();
    $(this).parent().next().slideDown();
    return false;
  });

}); 