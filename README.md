# TP1(student union)

## Pipeline Status
[![pipeline status](https://gitlab.com/ppwb_K1/ppwb-tp1/badges/master/pipeline.svg)](https://gitlab.com/ppwb_K1/ppwb-tp1/commits/master)

## Coverage Report
[![coverage report](https://gitlab.com/ppwb_K1/ppwb-tp1/badges/master/coverage.svg)](https:/https://gitlab.com/ppwb_K1/ppwb-tp1/commits/master)

Anggota Kelompok:

	1. Gusti Ngurah Yama Adi Putra
	2. Shafira Ishlah Nurulita 
	3. Karina Ivana 
    4. Andri Rahmadhan E

Link Herokuapp:
	http://ppwbk1.herokuapp.com/
