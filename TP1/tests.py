from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

class TP1UnitTest(TestCase):
    def test_urls_url_is_exist(self):
        #buat tes routing (urls)
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_urls_using_TP1_template(self):
        #buat tes /TP1 itu ngerender html apa
        response = Client().get('/')
        self.assertTemplateUsed(response, 'TP1.html')

    def test_urls_using_display_TP1_func(self):
        #ngecek ada fungsinya apa nggk
        found = resolve('/')
        self.assertEqual(found.func, views.display_TP1)

# Create your tests here.
