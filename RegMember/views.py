from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import registration_member
from .forms import Form_Registration_Member


# Fungsi-Fungsi di views 
regis_member = {}

def RegMember(request):
    regis_member['author'] = "Karina Ivana"
    regis_member['data_Member'] = registration_member.objects.all().order_by('-id')
    regis_member['form_Member'] = Form_Registration_Member
    return render(request, 'RegMember.html', regis_member)

def DataMember(request):
    
    registrationMember = Form_Registration_Member(request.POST or None)
    if (request.method == 'POST' and registrationMember.is_valid()):
        print('valid')
        regis_member['full_name'] = request.POST['full_name']
        regis_member['username'] = request.POST['username']
        regis_member['email'] = request.POST['email']
        regis_member['date_birth'] = request.POST['date_birth']
        regis_member['password'] = request.POST['password']
        regis_member['home_address'] = request.POST['home_address']
        daftarMember =registration_member(full_name = regis_member['full_name'], username = regis_member['username'], 
                        email = regis_member['email'], date_birth = regis_member['date_birth'], password = regis_member['password'], 
                        home_address = regis_member['home_address'])
        daftarMember.save()
        regis_member['data_Member'] = registration_member.objects.all()
        return HttpResponseRedirect('/Thanks/')
    else :
        return HttpResponseRedirect('/RegMember/')

def Thankyou(request):
    response = {}
    return render(request, 'Thankyou.html', response)