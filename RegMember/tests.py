from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import RegMember, DataMember, Thankyou
from .models import registration_member
from .forms import Form_Registration_Member


class RegMember_Test(TestCase):
    def test_urls_url_is_exist(self):
    #buat tes routing (urls)
        response = Client().get('/RegMember/')
        self.assertEqual(response.status_code,200)

    #buat tes /RegMember itu ngerender html apa
    def test_urls_using_RegMember_template(self):
        response = Client().get('/RegMember/')
        self.assertTemplateUsed(response, 'RegMember.html')

    def test_urls_using_display_reg_member_func(self):
    #ngecek ada fungsinya apa nggk
        found = resolve('/RegMember/')
        self.assertEqual(found.func, RegMember)

    #Mengecek Model dari App RegMember
    def test_new_regMember_Model(self):
        
        new_activity = registration_member.objects.create(full_name = "Karina Ivana", username = "Karina", email = "karina@example.com", date_birth = "2018-07-22", password = "secret", home_address = "Jl. Pertanian Selatan")

        counting_all_available_activity = registration_member.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    #Memberikan peringatan saat ada form yang belum terisi
    def test_form_validation_for_blank_items(self):
        form_invalid = Form_Registration_Member(data={'full_name': '', 'username' : "", 'email' : "", 'date_birth' : "", 'password' : '', 'home_address' : ''})
        self.assertFalse(form_invalid.is_valid())
        self.assertEqual(
            form_invalid.errors['full_name'],
            ["This field is required."]
        )

    #Mengecek placeholder dan css pada form
    def test_form_item_input_has_placeholder_and_css_classes(self):
        form = Form_Registration_Member()
        self.assertIn('placeholder="Full Name"', form.as_p())
        self.assertIn('placeholder="Username"', form.as_p())
        self.assertIn('placeholder="E-mail"', form.as_p())
        self.assertIn('placeholder="Date Birth"', form.as_p())
        self.assertIn('placeholder="Password"', form.as_p())
        self.assertIn('placeholder="Home Address"', form.as_p())
        self.assertIn('class="form-control"', form.as_p())

    #Mengecek saat terjadi Error saat post
    def test_form_registration_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response = Client().post('/RegMember/saveData/', {'full_name': '', 'username' : "", 'email' : "", 'date_birth' : "",'password' : '', 'home_address' : ''})
        self.assertEqual(response.status_code, 200)

        response= Client().get('/RegMember/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_form_registration_post_success_and_render_the_result(self):
        test ='Anonymous'
        form_data = {'full_name': test, 'username': test, 'email' : 'testing@gmail.com', 'date_birth' : '2018-02-02', 'password' : 'karinaivana', 'home_address' : 'jl pertanian selatan'}
        response_post = Client().post('/RegMember/saveData', form_data)
        self.assertEqual(response_post.status_code, 200)


    def test_using_method_post_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['full_name'] = "karina ivana"
        request.POST['username'] = "karina"
        request.POST['email'] = "haha@gmail.com"
        request.POST['date_birth'] = "2013-09-21"
        request.POST['password'] = "secret kali"
        request.POST['home_address'] = "Jl. Pramuka"
        DataMember(request)

    def test_using_method_post_not_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['full_name'] = ""
        request.POST['username'] = ""
        request.POST['email'] = ""
        request.POST['date_birth'] = ""
        request.POST['password'] = ""
        request.POST['home_address'] = ""
        DataMember(request)

    def test_Thanks_urls_url_is_exist(self):
    #buat tes routing (urls)
        response = Client().get('/Thanks/')
        self.assertEqual(response.status_code,200)

    #buat tes /RegMember itu ngerender html apa
    def test_urls_using_Thanks_template(self):
        response = Client().get('/Thanks/')
        self.assertTemplateUsed(response, 'Thankyou.html')

    def test_urls_using_display_Thanks_func(self):
    #ngecek ada fungsinya apa nggk
        found = resolve('/Thanks/')
        self.assertEqual(found.func, Thankyou)