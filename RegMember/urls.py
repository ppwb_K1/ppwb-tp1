from django.urls import path, include
from django.conf.urls import url
from django.contrib import admin
from . import views

#url for app
app_name = "RegMember"
urlpatterns = [
    url("RegMember/", views.RegMember, name = "Registration Mamber" ),
    url("saveData/", views.DataMember, name="DataRegistration"),
    url("Thanks/", views.Thankyou, name="Thank You") 
]