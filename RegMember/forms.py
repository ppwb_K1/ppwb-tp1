from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

class Form_Registration_Member(forms.Form):

    full_name = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Full Name',
        'cols': 50,
        'rows': 4,
        'id' :'id_full_name'
    }

    username = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Username',
        'cols': 50,
        'rows': 4,
        'id' :'id_username'
    }

    email = {
        'class' : 'form-control',
        'placeholder' : 'E-mail',
        'cols': 50,
        'rows': 4,
        'id' :'id_email'
    }

    date_birth = {
       'type' : 'date',
       'class' : 'form-control',
        'placeholder' : 'Date Birth',
        'cols': 50,
        'rows': 4,
        'id' :'id_date_birth'
    }

    password = {
        'class' : 'form-control',
        'placeholder' : 'Password',
        'cols': 50,
        'rows': 4,
        'id' :'id_password'
    }

    home_address = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Home Address',
        'cols': 50,
        'rows': 4,
        'id' :'id_home_address'
    }


    full_name = forms.CharField (max_length = 100, label = "Full Name ", widget = forms.TextInput(attrs = full_name))
    username = forms.CharField(max_length = 70, label = "User Name ", widget = forms.TextInput(attrs = username))
    email = forms.EmailField(max_length = 100, label = "E-mail ", widget= forms.EmailInput(attrs = email))
    date_birth = forms.DateField(label = "Date Birth ", input_formats= ['%Y-%m-%d'], widget = forms.DateInput(date_birth))
    password = forms.CharField(max_length = 70, min_length = 8, label = "Pasword ", widget=forms.PasswordInput(password))
    home_address = forms.CharField(max_length = 300, label = "Home Address ", widget = forms.TextInput(attrs = home_address))