from django.db import models


class registration_member(models.Model):
    full_name = models.CharField (max_length = 100)
    username = models.CharField(max_length = 70)
    email = models.EmailField(max_length = 100)
    date_birth = models.DateTimeField(max_length = 20)
    password = models.CharField(max_length = 70)
    home_address = models.CharField(max_length = 300)
