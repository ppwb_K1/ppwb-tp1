"""TugasPemprograman URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import TP1.urls as TP1
import News.urls as News
import Event.urls as Event
import RegParticipant.urls as RegParticipant
import RegMember.urls as RegMember
import About.urls as About

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include(TP1,namespace='TP1')),
    url(r'^News/', include(News,namespace='News')),
    url(r'^Event/', include(Event,namespace='Event')),
    url(r'^RegParticipant/', include(RegParticipant,namespace='RegParticipant')),
    url(r'', include(RegMember,namespace='Registration Member')),
    url(r'^About/', include(About,namespace='About')),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]  